FROM openjdk:11
ADD target/authorization-server-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080:8181
ENTRYPOINT ["java", "-jar", "app.jar"]
