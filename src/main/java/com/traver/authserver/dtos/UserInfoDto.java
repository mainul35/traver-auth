package com.traver.authserver.dtos;

import com.traver.authserver.models.UserModel;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserInfoDto extends BaseDto<UserModel> {
    private String id;
    private String username;
    private String displayName;
    private List<String> roles;
    private String email;
    private String password;
    private String jwtToken;
    private Date createdAt;
    private Date modifiedAt;

    public UserInfoDto() {
    }

    public void mapEntityToDto(UserModel UserModel) {
        this.setId (UserModel.getId ());
        this.setUsername (UserModel.getUsername ());
        this.setDisplayName (UserModel.getDisplayName ());
        this.setRoles (UserModel.getRoles ());
        this.setEmail (UserModel.getEmail ());
        this.setPassword ("******");
        this.setJwtToken (UserModel.getJwtToken ());
        this.setCreatedAt (UserModel.getCreatedAt ());
        this.setModifiedAt (UserModel.getUpdatedAt ());
    }
}
