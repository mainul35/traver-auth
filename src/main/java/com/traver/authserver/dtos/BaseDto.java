package com.traver.authserver.dtos;

public abstract class BaseDto<T> {

    public abstract void mapEntityToDto(T t);
}
