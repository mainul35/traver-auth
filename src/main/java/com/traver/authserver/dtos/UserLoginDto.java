package com.traver.authserver.dtos;

import lombok.Data;

@Data
public class UserLoginDto {
    private String username;
    private String password;
}
