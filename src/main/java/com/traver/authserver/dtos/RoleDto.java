package com.traver.authserver.dtos;

import com.traver.authserver.models.RoleModel;
import lombok.Data;

@Data
public class RoleDto extends BaseDto<RoleModel> {
    private String id;
    private String role;
    private String createdAt;
    private String modifiedAt;

    @Override
    public void mapEntityToDto(RoleModel RoleModel) {
        this.setId (RoleModel.getId ());
        this.setRole (RoleModel.getRole ());
        this.setCreatedAt (RoleModel.getCreatedAt ().toString ());
        this.setModifiedAt (RoleModel.getUpdatedAt ().toString ());
    }
}
