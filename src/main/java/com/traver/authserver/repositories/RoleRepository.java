package com.traver.authserver.repositories;

import com.traver.authserver.models.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface RoleRepository extends JpaRepository<RoleModel, String> {

    Optional<RoleModel> findById(String id);

    Optional<RoleModel> findByRole(String role);
}
