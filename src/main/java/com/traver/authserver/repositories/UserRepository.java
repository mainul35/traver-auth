package com.traver.authserver.repositories;

import com.traver.authserver.models.UserModel;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface UserRepository extends JpaRepository<UserModel, String> {

    @EntityGraph(attributePaths = "roles")
        // Handles lazy loading
    Optional<UserModel> findByUsername(String username);

    Optional<UserModel> findByJwtToken(String jwtToken);

    List<UserModel> findByUsernameOrEmail(String username, String email);

    Optional<UserModel> findByEmail(String email);
}
