package com.traver.authserver.models;

import com.traver.authserver.dtos.RoleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

@Data
@Entity(name = "roles")
@EqualsAndHashCode(callSuper = true)
public class RoleModel extends GenericModel {

    @Column
    private String role;

    public RoleModel() {
        super ();
    }

    public RoleModel(RoleDto roleDto) {
        this.setId (roleDto.getId () == null ? UUID.randomUUID ().toString () : roleDto.getId ());
        this.setRole (roleDto.getRole ());
    }
}
