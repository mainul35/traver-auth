package com.traver.authserver.models;

import com.traver.authserver.dtos.UserInfoDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.List;
import java.util.UUID;

@Data
@Entity(name = "users")
@EqualsAndHashCode(callSuper = true)
public class UserModel extends GenericModel {

    @Column(unique = true)
    private String username;

    @Column
    private String displayName;

    @Column(unique = true)
    private String email;

    @Column
    private String jwtToken;

    @Column
    private String password;

    @ElementCollection(fetch = FetchType.LAZY)
    private List<String> roles;

    public UserModel() {
        super ();
    }

    public UserModel(UserInfoDto userInfoDto) {
        this.setId (userInfoDto.getId () == null ? UUID.randomUUID ().toString () : userInfoDto.getId ());
        this.setEmail (userInfoDto.getEmail ());
        this.setDisplayName (userInfoDto.getDisplayName ());
        this.setUsername (userInfoDto.getUsername ());
    }
}
