package com.traver.authserver.config.security;

import com.traver.authserver.config.exceptions.AuthenticationException;
import com.traver.authserver.config.exceptions.InvalidTokenException;
import com.traver.authserver.models.UserModel;
import com.traver.authserver.repositories.RoleRepository;
import com.traver.authserver.repositories.UserRepository;
import com.traver.authserver.services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CustomAuthProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TokenService tokenService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        final String token = (String) usernamePasswordAuthenticationToken.getCredentials ();
        if (token.isEmpty ()) {
            return new User (username, "", AuthorityUtils.createAuthorityList ("ROLE_ANONYMOUS"));
        }
        Optional<UserModel> optionalUserModel = userRepository.findByUsername (username);

        if (optionalUserModel.isPresent ()) {
            UserModel UserModel = optionalUserModel.get ();
            try {
                tokenService.validateToken (UserModel.getJwtToken ());
            } catch (InvalidTokenException e) {
                UserModel.setJwtToken (null);
                userRepository.save (UserModel);
                return null;
            }

            return new User (username, "",
                    AuthorityUtils.createAuthorityList (UserModel.getRoles ().stream ().map (s -> "ROLE_" + s).toArray (String[]::new))
            );
        }
        throw new AuthenticationException ("No such user found with this username");
    }
}
