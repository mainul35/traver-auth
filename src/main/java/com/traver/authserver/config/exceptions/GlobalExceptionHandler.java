package com.traver.authserver.config.exceptions;

import org.hibernate.HibernateException;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Arrays;


@ControllerAdvice
//@DependsOn(value = {"loggerFactory"})
public class GlobalExceptionHandler {

    private final String duplicate = "Duplicate entry";
    private final String codeError = "Code Base Error";
    private final String notFound = "not_found";
    private final String serverError = "server_error";
    private final String badRequest = "invalid_request";
    private final String unAuthorized = "unauthorized";
    //TODO: add localized message

    Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> illegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.badRequest, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> nullPointerException(NullPointerException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.badRequest, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({HibernateException.class, SQLGrammarException.class})
    public ResponseEntity<?> hibernateExceptionHandler(HibernateException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.serverError, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ClassNotFoundException.class)
    public ResponseEntity<?> classNotFoundHandler(ClassNotFoundException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.codeError, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> globalExceptionHandler(DataIntegrityViolationException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.duplicate, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<> (errorResponse, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(DataRetrievalFailureException.class)
    public ResponseEntity<?> globalExceptionHandler(DataRetrievalFailureException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse (OffsetDateTime.now (), this.serverError, ex.getMessage (), request.getDescription (false));
        printStackTrace (ex);
        return new ResponseEntity<> (errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(AuthorizationServiceException.class)
    public ResponseEntity<?> globalExceptionHandler(AuthorizationServiceException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse (OffsetDateTime.now (), this.unAuthorized, ex.getMessage (), request.getDescription (false));
        printStackTrace (ex);
        return new ResponseEntity<> (errorResponse, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globalExceptionHandler(Exception ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse (OffsetDateTime.now (), this.serverError, ex.getMessage (), request.getDescription (false));
        printStackTrace (ex);
        return new ResponseEntity<> (errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<?> globalExceptionHandler(IOException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<?> globalExceptionHandler(ResponseStatusException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(OffsetDateTime.now(), this.notFound, ex.getMessage(), request.getDescription(false));
        printStackTrace(ex);
        return new ResponseEntity<>(errorResponse, ex.getStatus());
    }

    private void printStackTrace(Exception ex) {
        StackTraceElement[] trace = ex.getStackTrace();
        StringBuilder traceLines = new StringBuilder();
        traceLines.append("Caused By: ").append(ex.fillInStackTrace()).append("\n");
        Arrays.stream(trace).filter(f -> f.getClassName().contains("com.traver.authserver"))
                .forEach(traceElement -> traceLines.append("\tat ").append(traceElement).append("\n"));
        logger.error(traceLines.toString());
    }
}
