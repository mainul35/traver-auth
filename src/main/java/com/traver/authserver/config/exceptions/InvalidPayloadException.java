package com.traver.authserver.config.exceptions;

public class InvalidPayloadException extends RuntimeException {
    public InvalidPayloadException(String message) {
        super (message);
    }
}
