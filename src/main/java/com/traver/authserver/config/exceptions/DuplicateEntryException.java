package com.traver.authserver.config.exceptions;

public class DuplicateEntryException extends RuntimeException {
    public DuplicateEntryException(String s) {
        super (s);
    }

    public String getValue() {
        return super.getMessage ();
    }
}
