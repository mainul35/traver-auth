package com.traver.authserver.initialize;

public interface InitializeData {

    void initialize();
}
