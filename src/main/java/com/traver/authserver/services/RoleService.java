package com.traver.authserver.services;

import com.traver.authserver.dtos.RoleDto;

public interface RoleService {

    void createRole(RoleDto roleDto);

    RoleDto roleInfo(String roleId);
}
