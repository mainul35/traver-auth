package com.traver.authserver.services.impl;

import com.traver.authserver.config.exceptions.InvalidTokenException;
import com.traver.authserver.models.UserModel;
import com.traver.authserver.services.AuthSigninKeyResolver;
import com.traver.authserver.services.TokenService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private AuthSigninKeyResolver authSigninKeyResolver;

    @Override
    public void validateToken(String jwtToken) throws InvalidTokenException {
        try {
            Jwts.parserBuilder ()
                    .setSigningKeyResolver (authSigninKeyResolver)
                    .build ().parse (jwtToken);
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            throw new InvalidTokenException ("Invalid token", e);
        }
    }

    @Override
    public void generateToken(UserModel UserModel) {
        //TODO: Replace token with JWT
        String jwtToken = Jwts.builder ()
                .setSubject (UserModel.getUsername ())
                .setAudience (UserModel.getRoles ().toString ())
                .setIssuer (UserModel.getId ())
                .signWith (authSigninKeyResolver.getSecretKey (), SignatureAlgorithm.HS512)
                .compact ();
        UserModel.setJwtToken (jwtToken);
    }
}
