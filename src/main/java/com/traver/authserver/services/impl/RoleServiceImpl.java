package com.traver.authserver.services.impl;

import com.traver.authserver.dtos.RoleDto;
import com.traver.authserver.models.RoleModel;
import com.traver.authserver.repositories.RoleRepository;
import com.traver.authserver.services.RoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void createRole(RoleDto roleDto) {
        RoleModel RoleModel = new RoleModel (roleDto);
        roleRepository.save (RoleModel);
        roleDto.mapEntityToDto (RoleModel);
    }

    @Override
    public RoleDto roleInfo(String roleId) {
        Optional<RoleModel> optionalRoleModel = roleRepository.findById (roleId);
        if (optionalRoleModel.isPresent ()) {
            final RoleModel RoleModel = optionalRoleModel.get ();
            return modelMapper.map (RoleModel, RoleDto.class);
        }
        return null;
    }
}
