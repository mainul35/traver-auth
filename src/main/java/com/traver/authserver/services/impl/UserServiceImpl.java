package com.traver.authserver.services.impl;

import com.traver.authserver.dtos.UserInfoDto;
import com.traver.authserver.dtos.UserLoginDto;
import com.traver.authserver.models.RoleModel;
import com.traver.authserver.models.UserModel;
import com.traver.authserver.repositories.RoleRepository;
import com.traver.authserver.repositories.UserRepository;
import com.traver.authserver.services.TokenService;
import com.traver.authserver.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void createUser(UserInfoDto userInfoDto) {

        if (userRepository.findByUsername (userInfoDto.getUsername ()).isPresent ()) {
            throw new DuplicateKeyException ("Username is already registered");
        }

        if (userRepository.findByEmail (userInfoDto.getEmail ()).isPresent ()) {
            throw new DuplicateKeyException ("Email address is already registered");
        }
        // Transform userInfoDto to markdown role model
        UserModel UserModel = new UserModel (userInfoDto);

        // Hash the password first
        checkNotNull (userInfoDto.getPassword ());

        if (Optional.ofNullable (userInfoDto.getRoles ()).isEmpty ()) {
            userInfoDto.setRoles (Collections.singletonList ("USER"));
        }

        UserModel.setPassword (passwordEncoder.encode (userInfoDto.getPassword ()));

        // Assign default role for user
        UserModel.setRoles (
                roleRepository.findAll ().stream ()
                        .map (RoleModel::getRole)
                        .filter (role -> matchRole (role, userInfoDto.getRoles ()))
                        .collect (Collectors.toList ())
        );

        // Generate a new token for the user
        tokenService.generateToken (UserModel);

        // Save markdown role model
        userRepository.save (UserModel);

        // update the userInfoDto after the markdown role Model has been saved
        userInfoDto.mapEntityToDto (UserModel);
    }

    private boolean matchRole(String role, List<String> roles) {
        return roles.contains (role);
    }

    @Override
    public UserInfoDto retrieveUserInfo(String userId) {
        Optional<UserModel> UserModel = userRepository.findById (userId);
        if (UserModel.isPresent ()) {
            UserInfoDto userInfoDto = new UserInfoDto ();
            userInfoDto.mapEntityToDto (UserModel.get ());
            return userInfoDto;
        }
        return null;
    }

    @Override
    public UserInfoDto loginUser(UserLoginDto userLoginDto) {
        Optional<UserModel> optionalUserModel = userRepository.findByUsername (userLoginDto.getUsername ());
        if (optionalUserModel.isPresent ()) {
            UserModel UserModel = optionalUserModel.get ();
            if (passwordEncoder.matches (userLoginDto.getPassword (), UserModel.getPassword ())) {
                return modelMapper.map (UserModel, UserInfoDto.class);
            } else {
                throw new BadCredentialsException ("Password mismatched");
            }
        } else {
            throw new BadCredentialsException ("Invalid username");
        }
    }
}
