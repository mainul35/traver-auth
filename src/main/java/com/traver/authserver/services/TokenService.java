package com.traver.authserver.services;

import com.traver.authserver.config.exceptions.InvalidTokenException;
import com.traver.authserver.models.UserModel;

public interface TokenService {

    void validateToken(String jwtToken) throws InvalidTokenException;

    void generateToken(UserModel UserModel);
}
