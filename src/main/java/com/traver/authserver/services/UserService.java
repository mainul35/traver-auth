package com.traver.authserver.services;

import com.traver.authserver.dtos.UserInfoDto;
import com.traver.authserver.dtos.UserLoginDto;

public interface UserService {

    void createUser(UserInfoDto userInfoDto);

    UserInfoDto retrieveUserInfo(String userId);

    UserInfoDto loginUser(UserLoginDto userLoginDto);
}
