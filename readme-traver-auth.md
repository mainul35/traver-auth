## Start the application with docker compose

In the application we have written a shell script file for our convenience to start up the application.

From terminal, run the following command:

```
> sh install.sh
```

This command will do all the necessaty things
(e.g: running gradle clean build, removing old built image in local, new docker build, and startup with compose).

## Docker compose file description:

```
version: '3.8'
services:
  authorization-server:
    container_name: 'authorization-server'
    image: 'mainul35/traver-authorization-server'
    build: ./
    ports:
      - '8085:8085'
    depends_on:
      - postgresqldb
    command: mvn clean spring-boot:run
    # Pass environment variables to the service
    environment:
      # The inside port will be used by the dependent services
      SPRING_DATASOURCE_URL: jdbc:postgresql://postgresqldb:5432/traver_auth_db
      SPRING_DATASOURCE_USERNAME: postgres
      SPRING_DATASOURCE_PASSWORD: postgres
      SERVER_PORT: 8085
    networks:
      - traver
  postgresqldb:
    image: 'postgres'
    container_name: 'postgres'
    volumes:
    - type: volume
      source: my_dbdata
      target: "/home/mainul35/Documents/traver-data"
    ports:
    - '54310:5432'
    environment:
    - 'POSTGRES_PASSWORD=postgres'
    - 'POSTGRES_USER=postgres'
    - 'POSTGRES_DB=traver_auth_db'
    networks:
      - traver
volumes:
  my_dbdata:
networks:
  traver:

```

Note that we have attached a physical file folder to our database volume `/home/mainul35/Documents/traver-data`

## Connecting a database client to the containerized PostgreSQL

Let's check on which port our database is running:

```
mainul35@mainul35-HP-ZBook-15:~/Documents/traver-data$ sudo docker ps -a
[sudo] password for mainul35: 
CONTAINER ID   IMAGE                                  COMMAND                  CREATED             STATUS                  PORTS                                                 NAMES
2af418b15b7d   mainul35/traver-authorization-server   "java -jar app.jar m…"   About an hour ago   Up About an hour        0.0.0.0:8085->8085/tcp, :::8085->8085/tcp, 8181/tcp   authorization-server
2e84c1921bbb   postgres                               "docker-entrypoint.s…"   About an hour ago   Up About an hour        0.0.0.0:54310->5432/tcp, :::54310->5432/tcp           postgres
```

Notice that our Database's IP address is 0.0.0.0 and port is 54310 -> 5432. To connect a database client to out database
server, we will have to use port **54310** and IP Address **0.0.0.0**

We will use the same username and password that we provided in our docker-compose.yml file.
![img.png](images/connecting-db-client-to-containerized-db.png)
