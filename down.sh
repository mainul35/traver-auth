CONTAINER_NAMES=('postgres' 'authorization-server')
for i in "${CONTAINER_NAMES[@]}"
do
	CID=$(docker ps -q -f status=running -f name=^/$i$)
  if [ ! "${CID}" ]; then
    docker rm container $i
  else
    docker stop $CID
    docker rm container $CID
  fi
  unset CID
done

IMAGE_NAME='mainul35/traver-authorization-server'
if [ ! $(docker images -q "${IMAGE_NAME}") ]; then
  sudo docker image rm $(IMAGE_NAME)
fi
